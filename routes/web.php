<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/files', function () {

    // $messages = Message::all()->where('type', '=', 'chat');
    // $latest = Message::whereType('live')->latest()->first();
    $messages[]["text"] = "Hello One";
    $messages[]["text"] = "Hello Two";
    return inertia('File', [
        'messages' => $messages,
        // 'live' => $latest
    ]);
});
